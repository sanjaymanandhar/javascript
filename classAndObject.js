class Student {
    constructor(name, age, grade, email) {
        this.name = name;
        this.age = age;
        this.grade = grade;
        this.email = email;
    }
}

let sanjay = new Student("sanjay Manandhar", 22, "8th semester", "sanaymanandhar@bajratechnologies.com")
let alex = new Student("Alex shrestha", 22, "6th semester", "alex@bajratechnologies.com")