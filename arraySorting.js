const array = [5, 34, 67, 8, 1, 66]

//array sorting
const sort = (array) => {
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < array.length - 1; j++) {
            if (array[j] > array[j + 1]) {
                var temp_value = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp_value
            }
        }
    }
    console.log(array)
}

sort(array)
sort([3, 5, 7, 8, 44, 3, 6672, 34])

//map function returns a copy of new array

let doubleArray = array.map(i => i * 2)
console.log(doubleArray)