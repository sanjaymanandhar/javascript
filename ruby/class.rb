def Hello
    puts "hello world"
end

# method with a parameter
def name(name)
    puts "helllo #{name} from ruby"
end

Hello()
name("alex")