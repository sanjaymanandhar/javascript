puts "Hello world!!!" #output Hello world!!!

puts "Hello World\nFrom Ruby" #\n for new line

puts "Hello World\t From Ruby" #\n for horizontal space

print "Hello World" # puts add next line ad end of line while print does not 
print "From Ruby\n" # puts add next line ad end of line while print does not 

puts "Hello ","World","From Ruby" #comma also separates new line

#double quotation and single quotation
puts "Hello\nWorld"
puts "Hello\tWorld"
puts 'Hello\nWorld'
puts 'Hello\tWorld'

#string
first_name = "sanjay"
last_name = "manandhar"
email ="test@gmail.com"
puts first_name+" "+last_name+" "+email
puts "#{first_name} #{last_name} #{email}"

puts first_name.include?("S") #checks the given string
puts "hEllO ThIS is RANdom perSOn".downcase
puts "hEllO ThIS is RANdom perSOn".upcase


# arrays access
a= "Ruby is an OOP language".split(" ")
puts a.reverse()#reverses the array
puts a[0] #bracket notation to access particular elements

new_array = ["Coding",23,a.include?("OOP")]
puts new_array
puts new_array.slice(1,2)

#range to array
puts (0..10).to_a

#3.3.4 exercise
num = (1..10).to_a
puts num.slice(3,num.length)
puts num.slice(-7,num.length)

string = "ant bat cat"
puts string.slice(4,3) #gives output bat


new_string = ("a".."z").to_a
puts new_string.slice(0,13)
puts new_string.slice(0..12)

#array methods
a = [3,34,2,334,5,77,91]
puts a.sort
puts a.reverse

a.push(9)
a.push("new")
puts a
a.pop
puts a

#shovel operator to insert/pushing into arrays
a <<"man"<<"can"<<2
puts a

puts a.join(",") # joins the element of array with ,
 

#3.4.4 exercise
array = [1,2,3,4,5,6]
new_array = array.join(" ").split(" ")
puts array.join(" ").split(" ")
puts array == new_array # In particular, confirm using == that a.join(" ").split(" ")is not the same as a. because the join(" ") method creates the string whether the a contains integer values


